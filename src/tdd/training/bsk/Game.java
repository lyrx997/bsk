package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	private ArrayList<Frame> frameList = new ArrayList<Frame>();
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		// To be implemented
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if (frameList.size() > 10) {
			
			throw new BowlingException("Max number of frames reached");
			
		} else {
			
			this.frameList.add(frame);
			
		}
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		if (index > 9 || index < 0) {
			
			throw new BowlingException("Invalid frame index");
			
		} else {
			
			return frameList.get(index);	
			
		}
	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		
		this.firstBonusThrow = firstBonusThrow;
		
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		
		this.secondBonusThrow = secondBonusThrow;
		
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {

		return this.firstBonusThrow;
		
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		
		return this.secondBonusThrow;
		
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int score = 0;
		int i = 0;
		int bonus = 0;
		
		while ( i < frameList.size() ) {
			
			if ( frameList.get(i).isStrike() ) {
				
				bonus = calculateStrikeBonus(i);
				frameList.get(i).setBonus(bonus);
				
			} else if ( frameList.get(i).isSpare() ) {
				
				bonus = calculateSpareBonus(i);
				frameList.get(i).setBonus(bonus);
				
			}
			
			score += frameList.get(i).getScore();
			i++;
		}
		
		return score;	
	}
	
	/**
	 * It returns the Strike bonus of this frame.
	 * 
	 * @param index Index of current frame.
	 * @return Total Strike Bonus.
	 */
	private int calculateStrikeBonus(int index) {
		
		int bonus = 0;
		
		if ( index + 1 < frameList.size() && index != 8) {
			
			if ( frameList.get(index + 1).isStrike() && index + 2 < frameList.size() ) {
				
				bonus = frameList.get(index + 1).getFirstThrow() + frameList.get(index + 2).getFirstThrow();
				
			} else {
				
				bonus = frameList.get(index + 1).getFirstThrow() + frameList.get(index + 1).getSecondThrow();
				
				}

			} else if ( index == 8 && frameList.get(index + 1).isStrike() || index + 1 == frameList.size() ) {
				
				bonus = getFirstBonusThrow() + getSecondBonusThrow();
				
			}
		
	return bonus;
	
	}
	
	/**
	 * It returns the Spare bonus of this frame.
	 * 
	 * @param index Index of current frame.
	 * @return Total Spare Bonus.
	 */
	private int calculateSpareBonus(int index) {
		
		int bonus = 0;
		
		if ( index + 1 < frameList.size() ) {
			
			bonus = frameList.get(index + 1).getFirstThrow();
			frameList.get(index).setBonus(bonus);
			
		} else {
			
			bonus = getFirstBonusThrow();
			
		}
		
		return bonus;
	}
	

}
