package tdd.training.bsk;

public class Frame {
	
	private int firstThrow = 0;
	private int secondThrow = 0;
	private int bonus = 0;

	/**
	 * It initializes a frame given the pins knocked down in the first and second
	 * throws, respectively.
	 * 
	 * @param firstThrow  The pins knocked down in the first throw.
	 * @param secondThrow The pins knocked down in the second throw.
	 * @throws BowlingException
	 */
	public Frame(int firstThrow, int secondThrow) throws BowlingException {
		
		if (firstThrow + secondThrow > 10) {
			
			throw new BowlingException ("Invalid throw score");
			
		} else {
			
			this.firstThrow = firstThrow;
			this.secondThrow = secondThrow;
			
		}
		
	}

	/**
	 * It returns the pins knocked down in the first throw of this frame.
	 * 
	 * @return The pins knocked down in the first throw.
	 */
	public int getFirstThrow() {
		
		return this.firstThrow;
	}

	/**
	 * It returns the pins knocked down in the second throw of this frame.
	 * 
	 * @return The pins knocked down in the second throw.
	 */
	public int getSecondThrow() {

		return this.secondThrow;
	}

	/**
	 * It sets the bonus of this frame.
	 *
	 * @param bonus The bonus.
	 */
	public void setBonus(int bonus) {
		
		this.bonus = bonus;
		
	}

	/**
	 * It returns the bonus of this frame.
	 *
	 * @return The bonus.
	 */
	public int getBonus() {
		
		return this.bonus;
		
	}

	/**
	 * It returns the score of this frame (including the bonus).
	 * 
	 * @return The score
	 */
	public int getScore() {
		
		// A Strike is also a Spare, so it should work fine.
		if (this.isSpare()) {
			
			return this.firstThrow + this.secondThrow + this.bonus;
			
		} else {
			
			return this.firstThrow + this.secondThrow;
			
		}
			
	}

	/**
	 * It returns whether, or not, this frame is strike.
	 * 
	 * @return <true> if strike, <false> otherwise.
	 */
	public boolean isStrike() {

		return this.firstThrow == 10 && this.secondThrow == 0;
		
	}

	/**
	 * It returns whether, or not, this frame is spare.
	 * 
	 * @return <true> if spare, <false> otherwise.
	 */
	public boolean isSpare() {
		
		return this.firstThrow + this.secondThrow == 10;
		
	}

}
