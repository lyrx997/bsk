package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void testGetFrameAt() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(0, 0);
		Frame f2 = new Frame(2, 3);
		
		g.addFrame(f1);
		g.addFrame(f2);
		
		assertEquals(f2, g.getFrameAt(1));
	}
	
	@Test
	public void testCalculateScore() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(0, 0);
		Frame f2 = new Frame(2, 3);
		
		g.addFrame(f1);
		g.addFrame(f2);
		
		assertEquals(5, g.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithSpareFrame() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(5, 5);
		Frame f2 = new Frame(2, 3);
		
		g.addFrame(f1);
		g.addFrame(f2);
		
		assertEquals(17, g.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeFrame() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(10, 0);
		Frame f2 = new Frame(2, 3);
		
		g.addFrame(f1);
		g.addFrame(f2);
		
		assertEquals(20, g.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeFollowedBySpare() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(10, 0);
		Frame f2 = new Frame(4, 6);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);
		
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		
		assertEquals(103, g.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleStrikes() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(10, 0);
		Frame f2 = new Frame(10, 0);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);
		
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		
		assertEquals(112, g.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithMultipleSpares() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(8, 2);
		Frame f2 = new Frame(5, 5);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 6);
		
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		
		assertEquals(98, g.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithSpareFrameAsLastFrame() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(3, 6);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(2, 8);
		
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		
		g.setFirstBonusThrow(7);
		
		assertEquals(90, g.calculateScore());
	}
	
	@Test
	public void testCalculateScoreWithStrikeFrameAsLastFrame() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(1, 5);
		Frame f2 = new Frame(3, 6);
		Frame f3 = new Frame(7, 2);
		Frame f4 = new Frame(3, 6);
		Frame f5 = new Frame(4, 4);
		Frame f6 = new Frame(5, 3);
		Frame f7 = new Frame(3, 3);
		Frame f8 = new Frame(4, 5);
		Frame f9 = new Frame(8, 1);
		Frame f10 = new Frame(10, 0);
		
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		
		g.setFirstBonusThrow(7);
		g.setSecondBonusThrow(2);
		
		assertEquals(92, g.calculateScore());
	}
	
	@Test
	public void testCalculateBestScore() throws BowlingException {
		
		Game g = new Game();
		
		Frame f1 = new Frame(10, 0);
		Frame f2 = new Frame(10, 0);
		Frame f3 = new Frame(10, 0);
		Frame f4 = new Frame(10, 0);
		Frame f5 = new Frame(10, 0);
		Frame f6 = new Frame(10, 0);
		Frame f7 = new Frame(10, 0);
		Frame f8 = new Frame(10, 0);
		Frame f9 = new Frame(10, 0);
		Frame f10 = new Frame(10, 0);
		
		g.addFrame(f1);
		g.addFrame(f2);
		g.addFrame(f3);
		g.addFrame(f4);
		g.addFrame(f5);
		g.addFrame(f6);
		g.addFrame(f7);
		g.addFrame(f8);
		g.addFrame(f9);
		g.addFrame(f10);
		
		g.setFirstBonusThrow(10);
		g.setSecondBonusThrow(10);
		
		assertEquals(300, g.calculateScore());
	}

}
