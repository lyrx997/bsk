package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testGetFirstThrow() throws Exception{
		Frame f = new Frame(1, 2);
		
		assertEquals(1, f.getFirstThrow());
	}
	
	@Test
	public void testGetSecondThrow() throws Exception{
		Frame f = new Frame(1, 2);
		
		assertEquals(2, f.getSecondThrow());
	}
	
	@Test
	public void testGetScore() throws Exception {
		
		Frame f = new Frame(1, 2);
		
		assertEquals(3, f.getScore());
		
	}
	
	@Test
	public void testGetScoreIfSpare() throws Exception {
		
		Frame f = new Frame(5, 5);
		
		f.setBonus(3);
		
		assertEquals(13, f.getScore());
		
	}
	
	@Test
	public void testGetScoreIfStrike() throws Exception {
		
		Frame f1 = new Frame(10, 0);
		Frame f2 = new Frame(2, 3);
		
		f1.setBonus( f2.getFirstThrow() + f2.getSecondThrow() );
		
		assertEquals(15, f1.getScore());
		
	}
	
	@Test
	public void testIsSpare() throws Exception {
		
		Frame f = new Frame(5, 5);
		
		assertTrue(f.isSpare());
		
	}
	
	@Test
	public void testGetBonus() throws Exception {
		
		Frame f = new Frame(5, 5);
		
		f.setBonus(3);
		
		assertEquals(3, f.getBonus());
		
	}
	
	@Test
	public void testIsStrike() throws Exception {
		
		Frame f = new Frame(10, 0);
		
		assertTrue(f.isStrike());
		
	}
	

}
